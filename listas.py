#!/usr/bin/python3
# -*- coding: utf-8 -*-
#-------------------- Listas ----------------------

z=range(4)
print(z)
z=list(range(4))
print(z)
z=list(range(5,12,2))
print(z)


#------------------ Iteración --------------------
z=range(4)
for t in z:
	print(t)

for t in z:
	print("hola")

for t in z:
	print(t*t)


#--------------- Llenar una lista ----------------------

n=int(input("\n\nIngrese la longitud de la lista: "))
z=[None]*n

for a in range(len(z)):
	z[a]=float(input("Ingrese el valor de la posicion "+str(a)+": "))
print(z)



#--------------- Elegir tipo de dato ---------------------

n=int(input("\n\nIngrese la longitud de la lista: "))
z=[None]*n

for a in range(len(z)):
	n=int(input("\n[1]Numero\n[2]Cadena\n[3]Bool\nEliga un tipo para ingresar: "))
	if n==1:
		z[a] = float(input("Ingrese el valor de la posición "+str(a)+": "))
	elif n==2:
		z[a] = str(input("Ingrese el valor de la posición "+str(a)+": "))
	elif n==3:
		aux = int(input("Ingrese el valor de la posición "+str(a)+": "))
		z[a] = bool(aux)
	
print("\n\nLa lista contiene:")
for a in range(len(z)):
	print("En la posicion ",a,": ",z[a])



