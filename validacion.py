#!/usr/bin/python3
# -*- coding: utf-8 -*-

#Validadcióń de datos

print("Calculo del área de un circulo")

radio = float(input("Introduzca el radio: "))

if radio > 0:
    area = 3.1416*radio**2
    print("Area del circulo", area)
else:
    print("No se puede procesar")


print("\nCalculo del área de un triangulo")

b = float(input("Ingrese la base: "))
h = float(input("Ingrese la altura: "))

if b > 0 and h > 0:
    print("Area: ",(b*h)/2)
else:
    print("No se puede procesar")