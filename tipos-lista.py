#!/usr/bin/python3
# -*- coding: utf-8 -*-

n = int(input("Número de elementos: "))
li = list(range(n))

for k in li:
    tp = int(input("\nTipo de dato:\n[1] Float \n[2] Cadena\n[3] Boolean\n[4] Lista numerica\nEliga una opcion: "))
    if tp == 1:
        li[k] = float(input("Escriba un float: "))
    elif tp == 2:
        li[k] = str(input("Escriba un cadena: "))
    elif tp == 3:
        li[k] = bool(input("Escriba un booleano: "))
    elif tp == 4:
        m = int(input("Numero de elementos de la sublista: "))
        lon = list(range(m))
        for z in lon:
            lon[z] = float(input("Escribe el numero: "))
        li[k] = lon
    else:
        print("No existe esa opción")
    print(k+1, "Posicioes llenas de ",n)

print("Lista: ",li)