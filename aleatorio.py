#!/usr/bin/python3
# -*- coding: utf-8 -*-

import random

print("Numeros aleatorios")
print("Entre 0 y 1: ",random.random())
print("Entre 1 y 10: ",random.uniform(1,10))
print("Enteros entre 1 y 10: ",random.randint(1,10))
print("Pares entre 0 y 101: ",random.randrange(0,101,2))
print("Elegir una letra de una cadena: ",random.choice("abcdefghij"))

items = [1,2,3,4,5,6,7]
random.shuffle(items)
print("Elementos de una lista revueltos: ",items)

print("Porción aleateoria de una lista",random.sample([1,2,3,4,5],3))
