#!/usr/bin/python3
# -*- coding: utf-8 -*-

#------leer un archivo-----------
file = open("calif.txt","r")

for line in file:
    print(line)

file.close()

#-------agregar información-------
file = open("calif.txt","a")

msg = str(input("Escribe la nueva calificación: "))
file.write("\n"+msg)

file.close()


#--------Conteo de caracteres--------
file = open("log.txt","r")

c=0
for line in file:
    c += 1
    l = len(line)
    print("+",line,"* linea num = ",c," longitud de linea",l)

file.close()


#----------Escribir un archivo--------
file = open("newfile.txt","w")

msg1 = str(input("Escribe algo: "))
msg2 = str(input("Escribe algo de nuevo: "))

file.write("Hola mundo en el nuevo archivo\n")
file.write(msg1+"\n")
file.write(msg2+"\n")

file.close()


#-----------Procesado de datos-------
file = open("calif.txt","r")
acum = 0

for l in file:
    line = l.replace("\n","")

    if line.isdigit():
        num = float(line)
        acum += num
    else:
        print("no")

file.close()
print("Sumatoria: ",acum)