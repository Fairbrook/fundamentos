#-----------------EJEMPLOS DEL VIDEO------------------------

#******************** Ejemplo 1 *****************
d = {}

n = int(input("Cuantos elementos tendrá tu diccionario > "))
k = 0
while k<n:
    clave = str(input("clave > "))
    valor = float(input("Valor asociado a la clave > "))
    d[clave] = valor
    k+=1

print(d)


#******************** Ejemplo 2 *****************

edos = {'ags':1,'jal':14,'oax':20}

for keys,values in edos.items():
    print(keys)
    print(values)




#------------------ FUNCIONES DE LOS DICCIONARIOS ------------------

dic = dict(zip('abcd',[1,2,3,4]))
print(dic)

items = dic.items()
print(dic)

keys = dic.keys()
print(keys)

values = dic.values()
print(values)

dic2 = dic.copy()
print(dic2)

dic2.pop("a")
print("Diccionario1: ",dic)
print("Diccionario2: ",dic2)

valor = dic.get("b")
print(valor)

dic.clear()
print(dic)




#-------------------------- CODIGO DE COBRO ---------------------

inv = {"gansito":12, "papas":14.5, "lunetas":11}
flag = False
total = 0

for prod,val  in inv.items():
    print(prod,": $",val)

while flag!=True:
    opc = str(input("Escoga unproducto > "))
    if(len(opc)):
        if(inv.__contains__(opc)):
            total += inv.get(opc)
            print("Subtotal: $",total)
        else:
            print("Ese producto no existe. Continuemos...")
    else:
        print("--------------------------\nTotal: $",total)
        flag = True