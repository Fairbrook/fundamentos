#!/usr/bin/python3

def valid(contra):
    print('Introdujiste',contra)

    largo = len(contra)
    longitud = 0
    digito = 0
    mayus = 0
    minus = 0
    cmpl = 0
    especiales=["!","_","#","$","%","&"]

    if largo >= 10:
        longitud = 1

    for letra in contra:
        if letra in especiales:
            cmpl += 1
        if letra.islower():
            minus = 1
        if letra.isupper():
            mayus = 1
        if letra.isdigit():
            digito = 1
    
    if (longitud*digito*mayus*minus) == 1 and cmpl >= 1:
        return True
    else:
        return False
    return

cadena = str(input("Introduce la contraseña: "))
if(valid(cadena)):
    print("Contraseña valida")
else:
    print("Contraseña invalida")