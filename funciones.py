#!/usr/bin/python3

hola = [1,2,3,4,5]

#----Suma el contenido de la lista-----
def suma(lista):
    if isinstance(lista,list):
        result = 0
        for k in lista:
            result+=k
        return result
    else:
        return -1


#-----Numero de elementos -------
def numero(lista):
    if isinstance(lista,list):
        return len(lista)
    else:
        return -1


#------Minimo comun multiplo------
def mcm(num1, num2):
    result = 1
    if isinstance(num1,int) and isinstance(num2,int):
        
        while num1>1:
            if((num1%2)==0):
                num1/=2
                result *=2
                if((num2%2)==0):
                    num2/=2

            elif((num1%3)==0):
                num1/=3
                result *=3
                if((num2%3)==0):
                    num2/=3

            elif((num1%5)==0):
                num1/=5
                result *=5
                if((num2%5)==0):
                    num2/=5
            else:
                result*=num1
                num1=1

        
        while num2>1:
            if((num2%2)==0):
                num2/=2
                result *=2

            elif((num2%3)==0):
                num2/=3
                result *=3

            elif((num2%5)==0):
                num2/=5
                result *=5
            else:
                result*=num2
                num2=1
                
        return result
    else:
        return -1

print(suma(hola))
print(numero(22))
print(mcm("pedro",45))

