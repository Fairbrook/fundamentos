#!/usr/bin/python3
# -*- coding: utf-8 -*-

#Tipos de dato (Primera imagen)
n1 = int(input("Dato entero: "))
n2 = float(input("Dato flotante: "))
n3 = str(input("Dato cadena: "))
n4 = bool(input("Dato booleano solo True/False: "))

print("Int",n1)
print("Real",n2)
print("Cad",n3)
print("Bool",n4)

print("\n")
#Operaciones (Segunda imagen)
n1 = float(input("Dato flotante1: "))
n2 = float(input("Dato flotante2: "))
n3 = float(input("Dato flotante3: "))
n4 = float(input("Dato flotante4: "))
n5 = float(input("Dato flotante5: "))

x = n1+n2+n3+n4+n5
y = n1-n2-n3-n4-n5
z = n1*n2*n3*n4*n5
a = x/5

print("Suma: ",x,"\nDiferencia: ",y,"\nProducto: ",z,"\nPromedio: ",a)
